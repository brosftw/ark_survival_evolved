# ARK-Survival-Evolved

## Introduction
Docker build of an ARK Surivival Evolved Server  
Use `docker pull brosftw/ark-survival-evolved:latest` to pull the latest version of our ARK Image.  
This image uses the [arkmanager](https://github.com/FezVrasta/ark-server-tools) to manage ark!  


## Supported TAGS


`1.6.32` `latest` [Branch 1.6.32](https://git.brosftw.ch/BrosFTW/ARK-Survival-Evolved/src/1.6.32)  
`1.6.27`  [Branch 1.6.27](https://git.brosftw.ch/BrosFTW/ARK-Survival-Evolved/src/1.6.27)  


---
## Features
* Easy to install
* Easy to configure
* Easy crontab configuration
* Easy mod installment


---
## Usage
fast and easy to setup server:

download image   
`docker pull brosftw/ark-survival-evolved:latest`  

 
basic docker run  
`docker run -it -p 7778:7778 -p 7778/udp -p 27015:27016 -p 27015:27016/udp -p 32330:32330 -e SESSIONNAME="Your Server Name in Ark"  -v /my/persistant/directory/path:/ark --name Containername brosftw/ark-survival-evolved:latest`  


the way we run our ark server  
`docker run -it -p <IP>:7778:7778 -p <IP>:7778:7778/udp -p <IP>:27015:27016 -p <IP>:27015:27016/udp -p <IP>:32330:32330 -e SESSIONNAME="Servername" -e ADMINPASSWORD="adminpw" -e BACKUPONSTART=0 -e TZ="Europe/Zurich" -v /my/persistant/directory/path:/ark -m 12G --memory-reservation 6G --name <containername> brosftw/ark-survival-evolved:latest`  



| Docker run Options | Default Value | Possible Values | Impact |
| ------------- |:-------------:| -----:| -----:|
| -p IPADDR:Port:Port | serveraddr. | any IP | fixed IPADDR|
| -e SESSIONNAME="name" | ARKxxxx | any name | servername in ARK itself | 
| -d | none | none | console create wird nicht gezeigt |
| -e TZ="Your TIMEZONE" | UTC | Different Timezones like Europe/Zurich | change Timezone of your server|
| -v /my/Persistant/Directory/path/:/ark | none  | your path | change volume path |
| -e Adminpassword="your PW" | none | eveything | set your in game Adminpassword|
| --name xxx | Container ID | any name | set docker container name |

### Arkmanager Commands
All arkmanager commands can be found [here](https://github.com/FezVrasta/ark-server-tools#usage)!


check server status  
`docker exec <Containername> arkmanager status`  

start server update manuly  
`docker exec <Containername> arkmanager update --force`  

force save world  
`docker exec <Containername> arkmanager saveworld`  

start backup of your server manuly  
`docker exec <Containername> arkmanager backup`  

list Online Players  
`docker exec <Containername> arkmanager rconmd ListPlayers`  

install Mods  
`docker exec <Containername> arkmanager installmod <ModID>`  

update Mods  
`docker exec <Containername> arkmanager update --update-mods`  

## Install Mods

To install mods use the command  
`docker exec <Containername> arkmanager installmod <ModID>`  

Now you need to edit the arkmanager config file.  
`vi arkmanager.cfg`  

Scroll down to the mods and remove the #.  
Then insert your mod IDs  
`ark_GameModIds="mod ID, mod ID"`

### crontab

If you wish to configure your own automatic backup you can use the file in you persistant path  
`cd /my/persistant/directory/path`  

use your prefered editor to configure your crontab  
`vi crontab`  
example for your backup script  
`# backup the server at midnight`  
`0 0 * * * arkmanager backup`  

---
## Enviroment Variables
| ENV Variable | Default Value | Possible Values |
| ------------- |:-------------:| -----:|
| UID | 1000 | any possible uid |
| GID | 1000 | any possible gid |
| UPDATEONSTART | 1 | True = 1 False = 0 |
| BACKUPONSTART | 1 | True = 1 False = 0 |
| WARNONSTOP | 0 | True = 1 False = 0 |
| BACKUPONSTOP | 1 | True = 1 False = 0 |
| SERVERPASSWORD |  | A Secret Password |
| ADMINPASSWORD |  | A Secred Admin Password |
| SERVERMAP | TheIsland | Any possible ark map |
---
## Volumes
* /ark 
 * /arkmanager.cfg
 * /backup
 * /crontab
 * /Game.ini
 * /GameUserSettings.ini
 * /log
 * /server
 * /staging
 * /template

---
## Configuration files
Ark configuration files can be edited  

Arkmanager config (Mods must be added here as well)`arkmanager.cfg`  

Normal Ark config file`GameUserSettings.ini`  

If exra configs are needed`Game.ini`  


---
## Expose

* Port STEAMPORT: Steam port 7778
* Port SERPORT: server port 27016
* Port : 32330 : rcon port


---
## Contribution guidelines 

* Test the Containers
* Contact the [How to contribute](Link URL)
* Open a [Issue](https://git.brosftw.ch/BrosFTW/Minecraft/issues/new)

### Who do I talk to? ###

* Open an Issue @git.brosftw.ch

